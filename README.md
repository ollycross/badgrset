
# BadgrSet!
A javascript utility that allows you to leverage the browser parsing of `img` tag's `srcset` and `sizes` attributes to provide responsive image loading for your background images. 

Based heavily on Alexander Cleas' method from here https://aclaes.com/responsive-background-images-with-srcset-and-sizes/

## Usage

    import BadgrSet from 'badgrset'

    // One element
    new BadgrSet(document.getElementById('myElement');

    // Can't be bothered to write my own CSS, do it for me.
    BadgrSet.addStyles();

    // All elements matching '.badgrset'
    BadgrSet.scan()

    // A little extra CSS
    <style>
        .badgrset {
            height: 0;
            padding-bottom: 56.25%;
        }

    // Markup
    <div class="badgrset">
        <div class="some-class">Here is some awesome content"</div>
        <img
            class="badgrset-image"
            src="my-original-image.jpg"
            srcset="my-1200w-image.jpg 1200w,
                my-800w-image.jpg 800w,
                my-400w-image.jpg 400w"
            sizes="(min-width: 768px) 80vw,
                (min-width: 992px) 60vw,
                (min-width: 1200px) 50vw,
                100vw"
        >
    </div>

## Methods
__constructor (__ _HTMLElement_ element _string_ imgSelector = '.badgrset-image' __)__ _BadgrSet_
Creates a new instance of BadgrSet

- element _(required)_: The element on which the background will be applied.
- imgSelector: The CSS selector to find the image within the element _default_: `.badgrset-image`

__scan(__ _string|DOMElement_ containerSelector = 'body', _string_ itemSelector = '.badgrset', _string_ imgSelector = '.badgrset-image' __)__ _BadgrSet_
Scans a given container for elements matching `itemSelector`, creating a new _BadgrSet_ for each one found.  If string, uses `document.queryselector`. 

- containerSelector: CSS Selector for the containing element to scan _default_: `body`
- itemSelector: CSS Selector for the elements to have background images attached to _default_: `badgrset`
- imgSelector: The CSS selector to find the image within each element _default_: `.badgrset-image`

__addStyles__ __(__ _string_ itemSelector = '.badgrset', _string_ imgSelector = '.badgrset-image' __)__ _BadgrSet_
Appends as `style` element to the page with some basic styling (see notes).

- itemSelector: CSS Selector for the elements to have background images attached to _default_: `badgrset`
- imgSelector: The CSS selector to find the image within each element _default_: `.badgrset-image`



## Events
__badgrsetupdate__ Fires when the background-image src changes  `event.detail` contains `src`, the current src of the background-image.

_NB: If using jQuery you may need to look at event.originalEvent.detail_

## Notes
- This package is designed to handle the JS side of background images only - you will need to provide styles yourself.  At a minimum the '.badgrset-image' element should be hidden with `display: none`, and you will likely want to define some `background-*`, `height`, `padding` etc. CSS properties in your stylesheets.
- For convenience you can use the `BadgrSet.addStyles` function, which will append some basic styles to the head of the page:

      .badgrset {
        background-size: cover;
        background-position: center center;
        background-repeat: no-repeat;
       }
      .badgrset .badgrset-image {
        display: none !important;
      }
- This package is designed to be used with webpack and babel.  The `main` entry in package json points to the ES6 module in `/src`.  If this causes problems with your build process you can find the transpiled and bundled code in the `/dist` folder.