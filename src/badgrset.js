class BadgrSet {
  constructor(element, imgSelector = '.badgrset-image') {
    if (!element.badgrset) {
      element.badgrset = true;
      this.element = element;

      this.img = element.querySelector(imgSelector);
      this.src = '';

      this.update = this.update.bind(this);

      window.addEventListener('resize', this.update)
      this.img.addEventListener('load', this.update);

      if (this.img.complete) {
        this.update();
      }
    }
  }

  update() {
    let src = this.img.currentSrc || this.img.src;
    if (this.src !== src) {
      this.src = src;
      this.element.style.backgroundImage = `url('${this.src}')`;

      const event = new CustomEvent('badgrsetupdate', {
        bubbles: true,
        detail: {
          src: this.src
        }
      });
      this.element.dispatchEvent(event);

    }
  }
}

BadgrSet.addStyles = function(itemSelector = '.badgrset', imgSelector = '.badgrset-image') {
  const {
    head,
  } = document;
  const style = document.createElement('style');
  style.innerHTML = `${itemSelector} {\n` +
    'background-size: cover;\n' +
    'background-position: center center;\n' +
    'background-repeat: no-repeat;\n' +
    'display: inline-block\n' +
    '}\n'

  style.innerHTML += `${itemSelector} ${imgSelector} { display: none !important; }`;

  head.appendChild(style);
  return BadgrSet;
}

BadgrSet.scan = function(containerSelector = 'body', itemSelector = '.badgrset', imgSelector = '.badgrset-image') {
  const container = (typeof containerSelector === 'string') ? document.querySelectorAll(containerSelector) : [containerSelector];
  for (var i = container.length - 1; i >= 0; i--) {
    const elements = container[i].querySelectorAll(itemSelector);
    for (let x = 0; x < elements.length; x++) {
      new BadgrSet(elements[x], imgSelector);
    }
  }
  return BadgrSet;
}

export default BadgrSet;
